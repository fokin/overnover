//
//  main.cpp
//  OverNOver
//
//  Created on 2016 by Oleg Fokin (oleg.fokin@gmail.com)
//  Licensed under the WTFPL.
//
//  Problem with overloading and overriding mixture
//  Overriding - https://en.wikipedia.org/wiki/Method_overriding#C.2B.2B
//  Overloading - https://en.wikipedia.org/wiki/Function_overloading

#include <iostream>

class Base
{
public:
    virtual void overload() const   {std::cout << "void Base::overload() const\n";}
    virtual void overload()         {std::cout << "void Base::overload()\n";}
    virtual void overload(int) const{std::cout << "void Base::overload(int) const\n";}
    virtual void overload(int)      {std::cout << "void Base::overload(int)\n";}
};

class Derived : public Base
{
public:
    void overload() const override  {std::cout << "void Derived::overload() const\n";}
};

int main(int argc, const char * argv[])
{
    Derived d;
    d.overload();
    //d.overload(1); //compile error - overrided function hides other overloaded
    d.Base::overload(1);
    
    //correct polymorphism
    const Base& b = d;
    b.overload();
    b.overload(1);
    
    return 0;
}
